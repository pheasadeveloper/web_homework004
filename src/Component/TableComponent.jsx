import React, { Component } from 'react'

export default class TableComponent extends Component {
    render() {
        return (
            <div class="mt-4 accent-emerald-500 relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class=" w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-black-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            ID
                        </th>
                        <th scope="col" class="px-6 py-3">
                            USERNAME
                        </th>
                        <th scope="col" class="px-6 py-3">
                            AGE
                        </th>
                        <th scope="col" class="px-6 py-3">
                            ADDRESS
                        </th>
                        <th scope="col" class="px-6 py-3 row-span-2">
                            Action
                        </th>
                        <th scope="col" class="px-6 py-3 ">

                        </th>
                    </tr>
                </thead>
                <thead class="bg-purple-300 text-xs text-black-700 uppercase ">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            1
                        </th>
                        <th scope="col" class="px-6 py-3">
                            pheasa
                        </th>
                        <th scope="col" class="px-6 py-3">
                            22
                        </th>
                        <th scope="col" class="px-6 py-3">
                            pp
                        </th>
                        <th scope="col" class="px-6 py-3">
                            write
                        </th>
                        <th scope="col" class="px-6 py-3">
                            code
                        </th>
                    </tr>
                </thead>

                <thead class=" text-xs text-black-700 uppercase ">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            2
                        </th>
                        <th scope="col" class="px-6 py-3">
                            pheasa
                        </th>
                        <th scope="col" class="px-6 py-3">
                            22
                        </th>
                        <th scope="col" class="px-6 py-3">
                            pp
                        </th>
                        <th scope="col" class="px-6 py-3">
                            write
                        </th>
                        <th scope="col" class="px-6 py-3">
                            code
                        </th>
                    </tr>
                </thead>

            </table>
        </div>
        )
    }
}
